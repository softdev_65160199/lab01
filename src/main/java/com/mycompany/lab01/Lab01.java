/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.lab01;

/**
 *
 * @author sarit
 */
import java.util.Scanner;
import java.util.Random;
import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;
public class Lab01 {
        static ArrayList<Integer> playerPositions = new ArrayList<Integer>();
        static ArrayList<Integer> botPositions = new ArrayList<Integer>();
        public static void main(String[] args) {
            System.out.println("Welcome to OX Game !");
            char[][]gameBoard = {{' ', '|',' ','|',' '},
                {'-', '+', '-','+', '-'},
                {' ', '|', ' ','|', ' '},
                {'-', '+', '-','+', '-'},
                {' ', '|', ' ','|', ' '}};
            printGameBoard(gameBoard);
            
            
         
            while(true){
                Scanner scan = new Scanner(System.in);
                System.out.println("Enter input position (1-9):");
                int pos = scan.nextInt();
                while(playerPositions.contains(pos) || botPositions.contains(playerPositions)){
                    System.out.println("Correct Position");
                    pos = scan.nextInt();
                }
                placePiece(gameBoard, pos, "player");
                
                String result = checkwin();
                if(result.length()>0){
                    System.out.println(result);
                    break;
                }
                Random r = new Random();
                int botPos = r.nextInt(9)+1;
                while(playerPositions.contains(botPos) || botPositions.contains(botPositions)){
                    botPos = r.nextInt(9)+1;
                }
                placePiece(gameBoard, botPos, "bot");
                printGameBoard(gameBoard);
                result = checkwin();
                if(result.length()>0){
                    System.out.println(result);
                    break;
                }
                
            
            
            }
            
        }
        public static void printGameBoard(char[][] gameBoard){
            for(char[] row : gameBoard) {
                for(char c : row) {
                    System.out.print(c);
                }
                System.out.println();
        }
    }
    public static void placePiece(char[][]gameBoard, int pos, String user) {
    
        char A = ' ';
        if(user.equals("player")){
            A = 'X';
            playerPositions.add(pos);
        }else if(user.equals("bot")){
            A = 'O';
            botPositions.add(pos);
        }
            
        switch(pos) {
                case 1:
                    gameBoard[0][0] = A;
                    break;
                case 2:
                    gameBoard[0][2] = A;
                    break;
                case 3:
                    gameBoard[0][4] = A;
                    break;
                case 4:
                    gameBoard[2][0] = A;
                    break;
                case 5:
                    gameBoard[2][2] = A;
                    break;
                case 6:
                    gameBoard[2][4] = A;
                    break;
                case 7:
                    gameBoard[4][0] = A;
                    break;
                case 8:
                    gameBoard[4][2] = A;
                    break;
                case 9:
                    gameBoard[4][4] = A;
                    break;
                default:
                    break;
        }
    }
    
    public static String checkwin(){
        
        List TR = Arrays.asList(1,2,3);
        List MR = Arrays.asList(4,5,6);
        List BR = Arrays.asList(7,8,9);
        List LC = Arrays.asList(1,4,7);
        List MC = Arrays.asList(2,5,8);
        List BC = Arrays.asList(3,6,9);
        List C1 = Arrays.asList(1,5,9);
        List C2 = Arrays.asList(7,5,3);
        
        List<List>winner = new ArrayList<List>();
        winner.add(TR);
        winner.add(MR);
        winner.add(BR);
        winner.add(LC);
        winner.add(MC);
        winner.add(BC);
        winner.add(C1);
        winner.add(C2);
        
        
        for(List l:winner){
            if(playerPositions.containsAll(l)){
                return "Good! You winner";
            }else if(botPositions.containsAll(l)){
                return "Sad bot winner";
            }else if(playerPositions.size() + botPositions.size()==9){
                return "Tie";
            }
        }
        
        return "";
        
        
    }
}
